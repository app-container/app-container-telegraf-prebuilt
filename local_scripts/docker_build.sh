source ../container-name.sh
set -x
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
cp /workdir/build/container-x86-64-tig/tmp/deploy/images/container-x86-64/${IMAGE_NAME}.tar.bz2 ../dockerfile/${IMAGE_NAME}.tar.bz2
docker build --rm=true -t reslocal/${CONTAINER_NAME} ../dockerfile/
rm -f ../dockerfile/${IMAGE_NAME}.tar.bz2
set +x
